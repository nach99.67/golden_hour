import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_auth/src/bloc/login_bloc/bloc.dart';
import 'package:flutter_auth/src/bloc/authentication_bloc/bloc.dart';
import 'package:flutter_auth/src/repository/user_repository.dart';
import 'package:flutter_auth/src/ui/login/create_account_button.dart';
import 'package:flutter_auth/src/ui/login/google_login_button.dart';
import 'package:flutter_auth/src/ui/login/login_button.dart';

class LoginForm extends StatefulWidget {
  final UserRepository _userRepository;

  LoginForm({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  _LoginFormState createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  LoginBloc _loginBloc;

  UserRepository get _userRepository => widget._userRepository;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isLoginButtonEnabled(LoginState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _loginBloc = BlocProvider.of<LoginBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginBloc, LoginState>(listener: (context, state) {
      // tres casos, tres if:
      if (state.isFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(
            SnackBar(
              content: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [Text('Nooo me la contess, no se pudo iniciar sesión'), Icon(Icons.error)],
              ),
              backgroundColor: Colors.red,
            ),
          );
      }
      if (state.isSubmitting) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Logeando paa lo bueno se hace esperar... '),
                CircularProgressIndicator(),
              ],
            ),
          ));
      }
      if (state.isSuccess) {
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
      }
    }, child: BlocBuilder<LoginBloc, LoginState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.all(20.0),
          child: Form(
            child: ListView(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 0),
                  child: Image.asset(
                    'assets/golden-hour-icon-android.png',
                    height: 200,
                  ),
                ),
                TextFormField(
                  controller: _emailController,
                  cursorColor: Colors.deepOrange,
                  decoration: InputDecoration(
                      icon: Icon(Icons.email, color: Colors.white), labelText: 'Correo Electrónico'),
                  keyboardType: TextInputType.emailAddress, style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  // ignore: deprecated_member_use
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isEmailValid ? 'Introducí el correo para hoy' : null;
                  },
                ),
                TextFormField(
                  controller: _passwordController,
                  cursorColor: Colors.deepOrange,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock, color: Colors.white), labelText: 'Contraseña'),
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  obscureText: true,
                  // ignore: deprecated_member_use
                  autovalidate: true,
                  autocorrect: false,
                  validator: (_) {
                    return !state.isPasswordValid ? 'Acordate que es letra y numero paa' : null;
                  },
                ),
                Padding(
                  padding: EdgeInsets.symmetric(vertical: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      // Tres botones:
                      // LoginButton
                      LoginButton(
                        onPressed: isLoginButtonEnabled(state)
                            ? _onFormSubmitted
                            : null,
                      ),
                      // GoogleLoginButton

                      // CreateAccountButton
                      CreateAccountButton(
                        userRepository: _userRepository,
                      ),
                      GoogleLoginButton(),
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _onEmailChanged() {
    _loginBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _loginBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _loginBloc.add(LoginWithCredentialsPressed(
        email: _emailController.text, password: _passwordController.text));
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_auth/src/bloc/register_bloc/bloc.dart';
import 'package:flutter_auth/src/bloc/authentication_bloc/bloc.dart';
import 'package:flutter_auth/src/ui/register/register_button.dart';

class RegisterForm extends StatefulWidget {
  @override
  _RegisterFormState createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  // Dos variables
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();

  RegisterBloc _registerBloc;

  bool get isPopulated =>
      _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty;

  bool isRegisterButtonEnabled(RegisterState state) {
    return state.isFormValid && isPopulated && !state.isSubmitting;
  }

  @override
  void initState() {
    super.initState();
    _registerBloc = BlocProvider.of<RegisterBloc>(context);
    _emailController.addListener(_onEmailChanged);
    _passwordController.addListener(_onPasswordChanged);
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterBloc, RegisterState>(
        listener: (context, state) {
      // Si estado es submitting
      if (state.isSubmitting) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Registrando padreee ndeaa'),

                CircularProgressIndicator()
              ],
            ),
            backgroundColor: Colors.green,
          ));
      }
      // Si estado es success
      if (state.isSuccess) {
        BlocProvider.of<AuthenticationBloc>(context).add(LoggedIn());
        Navigator.of(context).pop();
      }
      // Si estado es failure
      if (state.isFailure) {
        Scaffold.of(context)
          ..hideCurrentSnackBar()
          ..showSnackBar(SnackBar(
            content: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text('Uhh algo salio mal, la baja una madness'),
                Icon(Icons.error)
              ],
            ),
            backgroundColor: Colors.red,
          ));
      }
    }, child: BlocBuilder<RegisterBloc, RegisterState>(
      builder: (context, state) {
        return Padding(
          padding: EdgeInsets.all(20),
          child: Form(
            child: ListView(
              children: <Widget>[
                // Un textForm para email
                TextFormField(
                  controller: _emailController,
                  cursorColor: Colors.deepOrange,
                  decoration: InputDecoration(
                    icon: Icon(Icons.email, color: Colors.white),
                    labelText: 'Correo electrónico',
                  ),
                  keyboardType: TextInputType.emailAddress,  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  autocorrect: false,
                  // ignore: deprecated_member_use
                  autovalidate: true,
                  validator: (_) {
                    return !state.isEmailValid ? 'Introducí el correo para hoy' : null;
                  },
                ),
                // Un textForm para password
                TextFormField(
                  controller: _passwordController,
                  cursorColor: Colors.deepOrange,
                  decoration: InputDecoration(
                      icon: Icon(Icons.lock, color: Colors.white), labelText: 'Contraseña'),
                  style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
                  obscureText: true,
                  autocorrect: false,
                  // ignore: deprecated_member_use
                  autovalidate: true,
                  validator: (_) {
                    return !state.isPasswordValid ? 'Acordate que es letra y numero paa' : null;
                  },
                ),
                // Un button
                RegisterButton(
                  onPressed:
                      isRegisterButtonEnabled(state) ? _onFormSubmitted : null,
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  void _onEmailChanged() {
    _registerBloc.add(EmailChanged(email: _emailController.text));
  }

  void _onPasswordChanged() {
    _registerBloc.add(PasswordChanged(password: _passwordController.text));
  }

  void _onFormSubmitted() {
    _registerBloc.add(Submitted(
        email: _emailController.text, password: _passwordController.text));
  }
}

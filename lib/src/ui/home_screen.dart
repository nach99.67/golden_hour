import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth/src/ui/photoUpload.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_auth/src/bloc/authentication_bloc/bloc.dart';


class Posts {
  String image, description, date, time;
  Posts(this.image, this.description, this.date, this.time);
}

class HomeScreen extends StatefulWidget {
  @override
  HomeScreenState createState() => HomeScreenState();
}


class HomeScreenState extends State<HomeScreen> {

  Future<Null> refreshList() async {
    await Future.delayed(Duration(seconds: 2));
  }

  List<Posts> postList = [];



  @override
  void initState(){
    super.initState();
  DatabaseReference postsRef =
        FirebaseDatabase.instance.reference().child("Posts");
  postsRef.once().then((DataSnapshot snap){
      var keys = snap.value.keys;
      var data = snap.value;

      postList.clear();

      for (var individualKey in keys) {
       Posts posts = Posts(
         data[individualKey]['image'],
         data[individualKey]['description'],
         data[individualKey]['date'],
         data[individualKey]['time'],
       );

       postList.add(posts);
      }

    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Image.asset('assets/banner-gh.png'),
        backgroundColor: Colors.grey[900],
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              BlocProvider.of<AuthenticationBloc>(context).add(LoggedOut());
            },
          )
        ],
      ),
      body: Container(

          child: postList.length == 0
              ? Center(child: Text('No hay blogs disponibles'),)
              : RefreshIndicator(
                onRefresh: refreshList,
                child: ListView.builder(
            itemCount: postList.length,
            itemBuilder: (_, index){
                return postsUI(
                    postList[index].image,
                    postList[index].description,
                    postList[index].date,
                    postList[index].time
                );
            },
          ),
              )

      ),
      bottomNavigationBar: BottomAppBar(
        color:Colors.deepOrange,
        child: Container(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              IconButton(
                icon: Icon(Icons.add_a_photo),
                iconSize: 40,
                color: Colors.white,
                onPressed: (){
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context){
                      return PhotoUpload();
                  }));
                },
              )
            ],
          ),
        ),
      ),
      backgroundColor: Colors.grey[900],
    );
  }

  Widget postsUI(String image, String description, String date, String time){
    return Card(
      elevation: 10.0,
      margin: EdgeInsets.all(14.0),
      child: Container(
        padding: EdgeInsets.all(14.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  date,
                  style: Theme.of(context).textTheme.subtitle2,
                  textAlign: TextAlign.center,
                ),
                Text(
                  time,
                  style: Theme.of(context).textTheme.subtitle2,
                  textAlign: TextAlign.center,
                )
              ],
            ),
            SizedBox(height: 10.0,),
            Image.network(
              image,
              fit: BoxFit.cover,
            ),
            SizedBox(height: 10.0,),
            Text(
              description,
              style: Theme.of(context).textTheme.subtitle1,
              textAlign: TextAlign.center,
            )
          ],
        ),
      ),
    );
  }
}


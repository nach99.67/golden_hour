import 'package:flutter/material.dart';
import 'package:flutter_auth/src/repository/user_repository.dart';
import 'package:flutter_auth/src/ui/register/register_screen.dart';

class CreateAccountButton extends StatelessWidget {
  final UserRepository _userRepository;

  CreateAccountButton({Key key, @required UserRepository userRepository})
      : assert(userRepository != null),
        _userRepository = userRepository,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      child: Text('Crearse una cuenta bien nasheee',
      style: TextStyle(
        fontSize: 14,

        color: Colors.black,




      )),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return RegisterScreen(
            userRepository: _userRepository,
          );
        }));
      },
    );

  }
}

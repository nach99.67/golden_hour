import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        fit: StackFit.expand,
        children: <Widget>[
          Container(
          decoration: BoxDecoration(color: Colors.deepOrange),
        ),
        Column(
          children: <Widget>[
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    CircleAvatar(
                      backgroundColor: Colors.deepOrange,
                      radius: 60.0,
                      child: Image.asset(
                        'assets/golden-hour-icon-android.png',
                        height: 1000,
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top:10.0),
                    ),
                    Text(
                      "Golden Hour", style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 24.0),
                    )
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  CircularProgressIndicator(backgroundColor: Colors.orangeAccent),
                  Padding(
                    padding: EdgeInsets.only(top: 20.0),
                  ),
                  Text("Golden Hour \n Es por aca rey",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.black,
                          fontSize: 18.0,
                      ),
                  ),
                ],
              ),
            )
          ],
        ),
        ],
      ),
    );
  }
}